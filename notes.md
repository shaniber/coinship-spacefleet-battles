# Coinship Spacefleet Battles

## Development Notes

### Simplify Turning
- change the capital ship's turn to 90 degrees to remove the need for measuring angles precisely. 

### Corvette Speed
- increase speed to 4cm.

### Distance Measurement
- change back to a string with knots every cm to measure moves and attacks.

### Modify Movement
- change to utilize only string.
- double all movement speeds.
- allow full 180 turns by all ships.

### Future Ideas
- implement as a TUI-based game as a practice project to learn Rust? 

