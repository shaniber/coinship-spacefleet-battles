# Coinship Spacefleet Battles

## A simple space battle simulator

Coinship Spacefleet Battles is a quick-and-dirty space battle simulator tabletop game. With the change in your pocket, you can enact the greatest space battles of history to come!

## The Basics
Required equipment:

- a handful of Canadian coins (loonies, toonies, and all the silver).
- a common 60� triangle ruler (a staple of grade school geometry kits).
- a pile of 6 sided dice (raid your Yahtzee sets and other games).

The game is played on a plain tabletop, with 2 players. Spacefleet size is agreed upon prior to starting. This can be a predetermined fleet composition, a money limit on change, or some other means of the players mutual agreement.

Play begins by placement of the spacefleet. This can be done by agreement between combatants, or random placement by taking your handful of change and dropping it from about 5cm from the tabletop. However determined, make sure that all of a player's ships stay on their half of the table.

Player one flips all their coins to heads, player 2 to tails. Place a d6 on each coin that has a hull value greater than 1, and set it to the hull number.

During a maneuver, players alternate moving ships to simulate the chaos of battle. Roshambo to determine who gets the initial move. Ships may be moved, turned, and make attacks in any order during a maneuver, and even split their move and attack. All ships must only move once per maneuver.

Attacks take place when ships are within weapons range, and can take place any time during a ship's move. Weapons automatically hit a targetted ship. When a ship is hit reduce its hull number by one. If a ship's hull number falls below 1, it is destroyed, and must be removed from the tabletop.

## Play
Here's an example of game play. First, the players decide on a Spacefleet size. After discussion, they agree on $2.50 as the maximum spending value. Player one (let's call them "Brady") decides to spend his money on a Capital Ship, a Corvette, and 3 Fighters. Player two (named, let's say, "Sara"), decides she wants two Cruisers, a Gunboat, and 5 fighters. They agree to place their tokens on their own side of the table in the order that they want, not crossing the imaginary centre line that splits the table.

To determine who goes first, Brady and Sara play a quick game of Roshambo. Sara throws rock, Brady throws scissors, so Sara goes first.

Sara starts the maneuver by moving one of her Cruisers. She takes her triangle, placing the zero point in the centre one of her loonies, and moves it 2cm towards Brady's spacefleet.

Next, Brady decides to move a fighter. After placing his triangle zero point at the centre of one of his nickles, he moves it 5cm towards Sara's side of the tabletop.

Sara sees Brady making his move with his Fighter, and decides to move her own Fighters up to protect her Cruiser. She does the same, moving a nickel 5cm towards Brady.

Play continues, each player alternating moving a ship. Once all of the ships have been moved, the current maneuver is over, and the next maneuver begins. Sara and Brady roshambo again, throwing rock and paper respectively, meaning that Brady moves first this maneuver. They move their ships closer, trying to get them into good positions to destroy one another's spacefleets.

As the game progresses, Brady's Corvette will come close enough to open fire on one of Sara's Cruisers. He moves 3cm, turns his dime 90�, and declares that he will fire his laser. It does 1 damage, so Sara changes the die on her nickel from 4 to 3. Brady then finishes moving his dime 2cm back towards his side of the table.

Sara decides to retaliate by firing a torpedo from her Gunboat at Brady's Capital Ship. Since she's already within 10cm, she declares this. However, since the Capital Ship has PDCs (Point Defence Cannons), it has a chance to destroy the torpedo before it hits. Brady takes a spare coin and flips it, calling heads while it spins in the air. It lands heads, meaning that Brady's PDCs have destroyed the torpedo before it hits, and as such takes no damage.

The battle continues, and eventually it comes down to Brady's badly damaged Capital Ship, and one of Sara's fighters. They Roshambo, Sara throwing Scissors, Brady throwing Paper, so Sara leads off this maneuver. She moves her nickel within 2cm of Brady's toonie, and declares her PDC attack. Brady looks at the die on his Capital Ship, and seeing that it's at 1, removes it from the board. Sara's won the battle of the spacefleets!

## Rules

### Maneuvers
A maneuver consists of moves and attacks. These can be done for each ship in your spacefleet. Attacks and moves can happen in any order, and you can split your moves and attacks at different times in the turn.

#### Moves
Use the geometry set triangle to measure movements. Measure movements from the centre of the coin. A ship can make one turn per movement, up to its turn rate. A ship can make turns any time during its movement. The minimum turn that can be made is 45�.

#### Attacks
Use the geometry set triangle to measure range. Measure range from edge to edge of the coins. An attack can be made at any point during a maneuver. Attacks can target different ships, as long as they're in range.

### Ships
Ships are the basis of your spacefleet. They can move every maneuver. They have a hull number, a movement rate, a turn rate, and several weapons.

#### capital ship
 cost | hull | move | turn | PDC       | laser     | torpedo
------|------|------|------|-----------|-----------|---------
 $2   | 5    | 1cm  | 45�  | 2/side    | 1 forward | 1

#### cruiser
 cost | hull | move | turn | PDC       | laser     | torpedo
------|------|------|------|-----------|-----------|---------
 $1   | 4    | 2cm  | 90�  | 1/side    | 1 forward | -
      |      |      |      |           | 1 aft     |

#### gunboat
 cost | hull | move | turn | PDC       | laser     | torpedo
------|------|------|------|-----------|-----------|---------
 �25  | 3    | 3cm  | 90�  | -         | 1 forward | 1
      |      |      |      |           | 1 aft     |
      |      |      |      |           | 1/side    |

#### corvette
 cost | hull | move | turn | PDC       | laser     | torpedo
------|------|------|------|-----------|-----------|---------
 �10  | 2    | 3cm  | 180� | -         | 1 forward | 2
      |      |      |      |           | 1 aft     |

#### fighter
 cost | hull | move | turn | PDC       | laser     | torpedo
------|------|------|------|-----------|-----------|---------
 �5   | 1    | 5cm  | 180� | 1 forward | -         | -

### Weapons
Weapons are used to perform attacks on the enemy spacefleet. They have a range, a damage rate, an attack rate, and restrictions or bonuses. Weapons automatically hit when their attack is declared, with the exception of torpedoes against a ship with a PDC.

#### PDC (point defence cannon)
 range  | damage | coverage | attack rate      | notes
 -------|--------|----------|------------------|------------------------------------
  2cm   | 1      | 60       | 1/turn           | can't damage hull at 4 or 5.
        |        |          |                  | can't drop capital ship hull to 0.
        |        |          |                  | 50% chance to destroy a torpedo.

#### laser
 range  | damage | coverage | attack rate      | notes
--------|--------|----------|------------------|------------------------------------
  5cm   | 1      | 90       | 1/turn           | can't drop capital ship hull to 0.

#### torpedo
 range  | damage | coverage | rate             | notes
--------|--------|----------|------------------|------------------------------------
  10cm  | 2      | 360�     | every other turn | can't hit fighter or corvette.


### Alternate Rules
If you've got coins that don't have an analog in the Canadian coin set, make something up for them! Gravitic mines, slow torpedoes, scout ships, use your imagination!

### Caveat
I'm sure that someone else has come up with a similar if not the same idea somewhere along the line. To be honest, I woke up in the middle of the night with this idea, so there's a really good chance that someone's done this before, and my subconscious mind is recalling it!

In fact, it may have been inspired by Zach Weinersmith's _Change Wars_: http://docs.google.com/Doc?docid=0ARFJ4OXV_SgwZGZwYnM5cnNfMzI0ZndrNmI0Y3I&hl=en


*Version 0.3.4 (Chaplin). Not ready for prime time.*
